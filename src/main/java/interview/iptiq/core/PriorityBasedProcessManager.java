package interview.iptiq.core;

import interview.iptiq.model.Process;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityBasedProcessManager extends DefaultProcessManager {
    private final Queue<Process> ascendingPriorityQueue;

    public PriorityBasedProcessManager(int capacity) {
        super(capacity);
        ascendingPriorityQueue = new PriorityQueue<>(capacity, Comparator.comparing(Process::getPriority));
    }

    /**
     * If the process is already added, this method returns.
     * <p>
     * If the process is new and there is enough capacity, then the process is added.
     * <p>
     * If the process is new and there isn't enough capacity and there is at least one process which has less priority than the given process,
     * then one of the low priority process(es) is removed and the new process is added.
     *
     * @param process The process to be added.
     */
    @Override
    public synchronized void add(Process process) {
        if (lookup.containsKey(process.getId())) {
            return;
        }
        if (lookup.size() < capacity) {
            ascendingPriorityQueue.offer(process);
            lookup.put(process.getId(), process);
            logger.info("Process with id " + process.getId() + " is added.");
        } else if (lookup.size() == capacity) {
            Process processWithLowestPriority = ascendingPriorityQueue.peek();
            if (process.getPriority() > processWithLowestPriority.getPriority()) {
                ascendingPriorityQueue.remove(processWithLowestPriority);
                lookup.remove(processWithLowestPriority.getId());
                ascendingPriorityQueue.offer(process);
                lookup.put(process.getId(), process);
                logger.info("Process with id " + process.getId() + " is added.");
            }
        }
    }

    @Override
    public synchronized void kill(long processId) {
        if (lookup.containsKey(processId)) {
            ascendingPriorityQueue.remove(lookup.get(processId));
        }
        super.kill(processId);
    }
}
