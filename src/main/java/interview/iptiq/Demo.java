package interview.iptiq;

import interview.iptiq.logging.StandardStreamLogger;
import interview.iptiq.model.Process;
import interview.iptiq.core.DefaultProcessManager;
import interview.iptiq.core.FIFOProcessManager;
import interview.iptiq.core.PriorityBasedProcessManager;
import interview.iptiq.core.ProcessManager;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Demo {
    private static final String LOG_SEPARATOR = "__________________________________________________\n";

    public static void main(String[] args) throws InterruptedException {
        demoSingleThreadProcessManager(new DefaultProcessManager(4));
        demoSingleThreadProcessManager(new FIFOProcessManager(4));
        demoSingleThreadProcessManager(new PriorityBasedProcessManager(4));

        demoMultiThreadProcessManager(new DefaultProcessManager(4));
        demoMultiThreadProcessManager(new FIFOProcessManager(4));
        demoMultiThreadProcessManager(new PriorityBasedProcessManager(4));
    }

    private static void demoSingleThreadProcessManager(ProcessManager processManager) {
        System.out.println(LOG_SEPARATOR + processManager.getClass().getName() + " -- Single Thread Demo");

        System.out.println(LOG_SEPARATOR + "Adding initial processes ...");
        processManager.addGroup(Arrays.asList(new Process(1L, 2, "/home/fakih/jdk/bin/java.exe"),
                new Process(2L, 7, "/home/fakih/jdk/bin/javac.exe"),
                new Process(3L, 9, "/home/fakih/jdk/bin/javadoc.exe")));

        System.out.println(LOG_SEPARATOR + "Listing processes, sorted by id: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getId)).forEach(System.out::println);

        processManager.add(new Process(4L, 1, "/home/fakih/jdk/bin/jar.exe"));

        System.out.println(LOG_SEPARATOR + "Listing processes after adding a new process, sorted by creation time: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getTimeCreated)).forEach(System.out::println);

        processManager.add(new Process(5L, 10, "/home/fakih/jdk/bin/jps.exe"));
        System.out.println(LOG_SEPARATOR + "Listing processes after trying to add a process when max capacity is hit, sorted by creation time: ");
        List<Process> processes = processManager.listRunningProcesses(Comparator.comparing(Process::getTimeCreated));
        processes.forEach(System.out::println);

        Process toBeKilled = processes.get(ThreadLocalRandom.current().nextInt(processes.size()));
        processManager.kill(toBeKilled.getId());
        System.out.println(LOG_SEPARATOR + "Details of the killed random process: \n" + toBeKilled);

        System.out.println(LOG_SEPARATOR + "Listing processes after killing a random process, sorted by priority: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getPriority)).forEach(System.out::println);

        processManager.killGroup(processManager.listRunningProcesses().stream().filter(process -> process.getExecutablePath().contains("java")).collect(Collectors.toList()));
        System.out.println(LOG_SEPARATOR + "Listing processes after killing the processes having 'java' in their executable paths, sorted by executable path: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getExecutablePath)).forEach(System.out::println);

        processManager.killAll();
        System.out.println(LOG_SEPARATOR + "Listing processes after killing all processes: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getPriority)).forEach(System.out::println);

        System.out.print("\n\n\n");
    }


    private static void demoMultiThreadProcessManager(ProcessManager processManager) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        processManager.setLogger(new StandardStreamLogger());

        System.out.println(LOG_SEPARATOR + processManager.getClass().getName() + " -- Multi Thread Demo");

        Runnable threadTryingToAddSameProcessMultipleTimes = () -> {
            Process process = new Process(1L, 2, "/home/fakih/jdk/bin/java.exe");
            IntStream.range(0, 10).forEach(i -> processManager.add(process));
        };
        Runnable secondAdder = () -> processManager.add(new Process(2L, 7, "/home/fakih/jdk/bin/javac.exe"));
        Runnable thirdAdder = () -> processManager.add(new Process(3L, 9, "/home/fakih/jdk/bin/javadoc.exe"));
        Runnable fourthAdder = () -> processManager.add(new Process(4L, 1, "/home/fakih/jdk/bin/jar.exe"));
        Runnable fifthAdder = () -> processManager.add(new Process(5L, 10, "/home/fakih/jdk/bin/jps.exe"));
        Runnable randomProcessKiller = () -> {
            List<Process> processes = processManager.listRunningProcesses();
            if (!processes.isEmpty()) {
                processManager.kill(processes.get(ThreadLocalRandom.current().nextInt(processes.size())).getId());
            }
        };
        List<Runnable> threads = Arrays.asList(threadTryingToAddSameProcessMultipleTimes, secondAdder, thirdAdder, fourthAdder, fifthAdder, randomProcessKiller);
        Collections.shuffle(threads);
        threads.forEach(executor::submit);

        Thread.sleep(2000);
        System.out.println(LOG_SEPARATOR + "Listing processes, sorted by id: ");
        processManager.listRunningProcesses(Comparator.comparing(Process::getId)).forEach(System.out::println);

        System.out.print("\n\n\n");
        Thread.sleep(2000);
        executor.shutdownNow();
    }
}
