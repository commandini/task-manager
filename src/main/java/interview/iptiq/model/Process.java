package interview.iptiq.model;

import java.util.concurrent.atomic.AtomicBoolean;

public class Process {
    private final long id;
    /**
     * Higher priority value means higher priority of the process.
     */
    private final int priority;
    private final long timeCreated;
    private final AtomicBoolean alive;
    private final String executablePath;

    public Process(long id, int priority, String executablePath) {
        this.id = id;
        this.priority = priority;
        this.executablePath = executablePath;
        alive = new AtomicBoolean(true);
        timeCreated = System.currentTimeMillis();
    }

    public long getId() {
        return id;
    }

    public int getPriority() {
        return priority;
    }

    public long getTimeCreated() {
        return timeCreated;
    }

    public String getExecutablePath() {
        return executablePath;
    }

    public boolean kill() {
        return alive.compareAndSet(true, false);
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", priority=" + priority + ", timeCreated=" + timeCreated + ", alive=" + alive + ", execPath='" + executablePath + '\'' + '}';
    }
}
