package interview.iptiq.core;

import interview.iptiq.exception.InvalidCapacityException;
import interview.iptiq.logging.Logger;
import interview.iptiq.model.Process;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class DefaultProcessManager implements ProcessManager {
    protected final int capacity;
    protected final Map<Long, Process> lookup;
    protected Logger logger;

    public DefaultProcessManager(int capacity) {
        if (capacity <= 0) {
            throw new InvalidCapacityException("Capacity must be a positive integer value.");
        }
        this.capacity = capacity;
        lookup = new ConcurrentHashMap<>();
        logger = new Logger() { /** void logger is used by default. **/
        };
    }

    @Override
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * If the process is already added, this method returns.
     * <p>
     * If the process is new and there's enough capacity, then the process is added.
     *
     * @param process The process to be added.
     */
    @Override
    public synchronized void add(Process process) {
        if (lookup.containsKey(process.getId())) {
            return;
        }
        if (lookup.size() < capacity) {
            lookup.put(process.getId(), process);
            logger.info("Process with id " + process.getId() + " is added.");
        }
    }

    @Override
    public synchronized void addGroup(Collection<Process> group) {
        for (Process process : group) {
            add(process);
        }
    }

    @Override
    public List<Process> listRunningProcesses() {
        return new ArrayList<>(lookup.values());
    }

    @Override
    public List<Process> listRunningProcesses(Comparator<Process> comparator) {
        List<Process> result = listRunningProcesses();
        Collections.sort(result, comparator);
        return result;
    }

    @Override
    public synchronized void kill(long processId) {
        if (lookup.containsKey(processId)) {
            lookup.get(processId).kill();
            lookup.remove(processId);
            logger.info("Process with id " + processId + " is killed.");
        }
    }

    @Override
    public synchronized void killGroup(Collection<Process> group) {
        Set<Long> processIds = group.stream().map(Process::getId).collect(Collectors.toSet());
        processIds.forEach(this::kill);
    }

    @Override
    public synchronized void killAll() {
        killGroup(lookup.values());
    }
}
