package interview.iptiq.core;

import interview.iptiq.logging.Logger;
import interview.iptiq.model.Process;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ProcessManager {
    /**
     * Adds the given process to the underlying data collection(s) managed by this ProcessManager.
     *
     * @param process The process to be added.
     */
    void add(Process process);

    /**
     * Adds the given processes to the underlying data collection(s) managed by this ProcessManager.
     *
     * @param group A group of Process objects to be added.
     */
    void addGroup(Collection<Process> group);

    /**
     * @return A list of all running processes managed by this ProcessManager.
     */
    List<Process> listRunningProcesses();

    /**
     * @param comparator A comparator of Process objects.
     * @return A list of all running processes managed by this ProcessManager, sorted by the given comparator.
     */
    List<Process> listRunningProcesses(Comparator<Process> comparator);

    /**
     * Invokes the kill() method of the given process
     * and removes it from the underlying data collection(s) managed by this ProcessManager.
     *
     * @param processId
     */
    void kill(long processId);

    /**
     * Invokes the kill() method of all the processes in the given group
     * and removes them from the underlying data collection(s) managed by this ProcessManager.
     *
     * @param group A group of Process objects.
     */
    void killGroup(Collection<Process> group);

    /**
     * Invokes the kill() method of all the processes in the underlying data collection(s) managed by this ProcessManager.
     * Clears the underlying data collection(s) respectively.
     */
    void killAll();

    /**
     * Sets the logger to be used for this ProcessManager.
     *
     * @param logger Logger instance.
     */
    void setLogger(Logger logger);
}
