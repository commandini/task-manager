package interview.iptiq.core;

import interview.iptiq.model.Process;

import java.util.*;

public class FIFOProcessManager extends DefaultProcessManager {
    private final Deque<Process> processQueue;

    public FIFOProcessManager(int capacity) {
        super(capacity);
        processQueue = new ArrayDeque<>();
    }

    /**
     * If the process is already added, this method returns.
     * <p>
     * If the process is new and there is enough capacity, then the process is added.
     * <p>
     * If the process is new and there isn't enough capacity,
     * then the oldest process in the queue is removed and the new process is added.
     *
     * @param process The process to be added.
     */
    @Override
    public synchronized void add(Process process) {
        if (lookup.containsKey(process.getId())) {
            return;
        }
        if (lookup.size() < capacity) {
            processQueue.add(process);
            lookup.put(process.getId(), process);
            logger.info("Process with id " + process.getId() + " is added.");
        } else if (lookup.size() == capacity) {
            Process removed = processQueue.removeFirst();
            lookup.remove(removed.getId());
            processQueue.add(process);
            lookup.put(process.getId(), process);
            logger.info("Process with id " + process.getId() + " is added.");
        }
    }

    @Override
    public synchronized void kill(long processId) {
        if (lookup.containsKey(processId)) {
            processQueue.remove(lookup.get(processId));
        }
        super.kill(processId);
    }
}
