# Task Manager

A simple process manager implementation.

# Build Notes

The project uses Gradle build system.
Minimum Java version required: 11

Once you cloned the Git repository or extracted the project archive to a folder of your choice,
navigate to the project root folder and run the following command in order to build the project.

````shell
# On Windows systems
>  .\gradlew.bat clean build

# On Linux systems
>  ./gradlew clean build
````

Once the building is successful, you can navigate to this folder under the project root: build\classes\java\main
Then you can run the following command for demonstration:

```shell
> java -cp . interview.iptiq.Demo
```

Alternatively, you can open the project in an IDE like IntelliJ IDEA and all the operations/settings described above can
be performed in much easier ways.

# Process Model

The process model has 4 fields based on the assignment:

- explicit fields -> PID and priority
- implicit fields -> state(alive/dead), creation time

I've added executablePath field for the purpose of demonstrating killGroup(),
there was no intention to bring a complete real-world process model whatsoever, it's a basic model.

# Demo

Demo class is added to showcase how different implementations of ProcessManager behave, given the similar set of
operations. It's verbose, however I'd use such code only for debugging purposes.

You can run it through main method and observe its behaviour via standard output.
Single-thread and multiple-thread scenarios are handled with different methods in Demo class, you can customize them and
run/debug my ProcessManager API.

# Testing

Testing is very important, however I'm leaving it out of this homework assignment due to lack of time.
A few notes on how my tests would look like:

- API tests with single thread would focus on various ordering of a set of API calls and then assessing the expected
  results.
  For example, think of following 5 operations:
  Add process 1, Add processes {6,7,8}, Remove process 1, List All, Add process 4

  There are 5! = 120 different orderings of these 5 operations. Sometimes, swapping 2 operations in a given ordering
  wouldn't change the result.Yet, given the high number of API call orderings (even for a simple example),
  I'd focus on some distinct scenarios and cover them properly.
- API tests with multiple threads is a much more complex topic. I'd introduce multiple threads performing various API
  calls and set-up test scenarios accordingly.
  Some cases to test: Checking a task manager never exceeds its capacity, ensuring read-only calls don't block and
  display consistent data. Basically, the tests would focus on checking the various states of the shared resources under
  a selected set of scenarios.

