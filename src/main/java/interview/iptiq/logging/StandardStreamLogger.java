package interview.iptiq.logging;

public class StandardStreamLogger implements Logger {

    @Override
    public void info(String message) {
        System.out.println(message);
    }
}
